import os
import boto3
from dotenv import load_dotenv
import pprint
load_dotenv()

#De credentials worden genomen vanuit ".env" file en worden gebruikt om de verbinding te maken met AWS Cloud Platform
access_key=os.getenv('AWS_ACCESS_KEY')
secret_key=os.getenv('AWS_SECRET_KEY')
region_name=os.getenv('AWS_Region')

#"init_aws_session" verzamelt de credentials die later zullen gebruikt worden om vpc.session te maken.
def init_aws_session():
    return boto3.Session(aws_access_key_id=access_key, aws_secret_access_key=secret_key, region_name=region_name)

#"ec2_get_vpc_list" geef volledige overzicht van alle vpc's binnen AWS Cloud Platform. Helaas is het niet gelukt om alleen CIDR en VPC ID te vermelden. Tijdens de uitwerking van deze onderdeel moest er "IAM" aangepast worden maar ik had er geen toegang tot.
def ec2_get_vpc_list():
    session = init_aws_session()
    ec2 = session.client('ec2')
    response = ec2.describe_vpcs()
    return response['Vpcs']

# Binnen "vpc" wordt er een overzicht van de bestaande VPC's getoond net als nieuwe vpc's gemaakt. 
def vpc(vpcname,cidrvpc,cidrsubnet):
    ec2 = boto3.resource('ec2',aws_access_key_id=access_key, aws_secret_access_key=secret_key, region_name=region_name)
    vpc_list = ec2_get_vpc_list()
    pprint.pprint(vpc_list)

    vpc = ec2.create_vpc(CidrBlock= cidrvpc)
    # Een naam aan VPC toewijzen.
    vpc.create_tags(Tags=[{"Key": "Name", "Value": vpcname + '-vpc'}])
    vpc.wait_until_available()

    # Openbare DNS-hostnaam wordt aangeschakeld zodat SHH later gebruikt kunnen worden. 
    ec2Client = boto3.client('ec2',region_name=region_name)
    ec2Client.modify_vpc_attribute( VpcId = vpc.id , EnableDnsSupport = { 'Value': True } )
    ec2Client.modify_vpc_attribute( VpcId = vpc.id , EnableDnsHostnames = { 'Value': True } )

    # Maakt een internetgateway en koppel deze aan VPC
    internet_gateway = ec2.create_internet_gateway()
    vpc.attach_internet_gateway(InternetGatewayId=internet_gateway.id)

    # Maakt een routetabel en een openbare route
    create_routetable = vpc.create_route_table()
    route = create_routetable.create_route(DestinationCidrBlock='0.0.0.0/0', GatewayId=internet_gateway.id)

    # Maakt een subnet en koppel het aan de routetabel
    subnet = ec2.create_subnet(CidrBlock= cidrsubnet, VpcId=vpc.id)
    create_routetable.associate_with_subnet(SubnetId=subnet.id)


    # Maak een beveiligingsgroep en sta SSH-inkomende regel toe via de VPC
    security_group = ec2.create_security_group(GroupName='SSH-ONLY', Description='only allow SSH traffic', VpcId=vpc.id)
    security_group.authorize_ingress(CidrIp='0.0.0.0/0', IpProtocol='tcp', FromPort=22, ToPort=22)
    print('Done')

