import boto3
from dotenv import load_dotenv
import os
import pprint
from def_lib import ec2_get_vpc_list
load_dotenv()
access_key=os.getenv('AWS_ACCESS_KEY')
secret_key=os.getenv('AWS_SECRET_KEY')
region_name=os.getenv('AWS_Region')
ec2 = boto3.resource('ec2',aws_access_key_id=access_key, aws_secret_access_key=secret_key, region_name=region_name)
vpc_list = ec2_get_vpc_list()
pprint.pprint(vpc_list)
print('Please choose of the opties')

text = input('To create own VPC , write Yes/No : ')
if text == "Yes" or text == "yes":
# Binnen "vpc" wordt er een overzicht van de bestaande VPC's getoond net als nieuwe vpc's gemaakt. 
    vpcname = input('Give your VPC name : ')
    subnet = input('Give your subnet name : ')
    internet_gateway = input('Give your internet_gateway name : ')
    route_table = input('Give your route-table name : ')
    cidrvpc = input('Give your CidrBlock (Ex. 172.16.0.0/16) : ')
    cidrsubnet = input('Give your CidrBlock (Ex. 172.16.1.0/24) : ')

    vpc = ec2.create_vpc(CidrBlock= cidrvpc)

    # Een naam aan VPC toewijzen.
    vpc.create_tags(Tags=[{"Key": "Name", "Value": vpcname + '-vpc'}])
    vpc.wait_until_available()

    # Openbare DNS-hostnaam wordt aangeschakeld zodat SHH later gebruikt kunnen worden. 
    ec2Client = boto3.client('ec2',region_name=region_name)
    ec2Client.modify_vpc_attribute( VpcId = vpc.id , EnableDnsSupport = { 'Value': True } )
    ec2Client.modify_vpc_attribute( VpcId = vpc.id , EnableDnsHostnames = { 'Value': True } )

    # Maakt een internetgateway en koppel deze aan VPC
    internet_gateway = ec2.create_internet_gateway(TagSpecifications=[
            {
                'ResourceType': 'internet-gateway',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': internet_gateway+'-igw'
                    },
                ]
            },
        ])
    vpc.attach_internet_gateway(InternetGatewayId=internet_gateway.id)

    # Maakt een routetabel en een openbare route
    create_routetable = vpc.create_route_table(TagSpecifications=[
                {
                    'ResourceType': 'route-table',
                    'Tags': [
                        {
                            'Key': 'Name',
                            'Value': route_table+'-rt'
                        },
                    ]
                },
            ])
    route = create_routetable.create_route(DestinationCidrBlock='0.0.0.0/0', GatewayId=internet_gateway.id)

    # Maakt een subnet en koppel het aan de routetabel
    subnet = ec2.create_subnet(CidrBlock= cidrsubnet, VpcId=vpc.id , TagSpecifications=[
            {
                'ResourceType': 'subnet',
                'Tags': [{
                    'Key': 'Name',
                    'Value': subnet+'-subnet'
                }]
            },
        ],)
    create_routetable.associate_with_subnet(SubnetId=subnet.id)


    # Maak een beveiligingsgroep en sta SSH-inkomende regel toe via de VPC
    security_group = ec2.create_security_group(GroupName='SSH-ONLY', Description='only allow SSH traffic', VpcId=vpc.id)
    security_group.authorize_ingress(CidrIp='0.0.0.0/0', IpProtocol='tcp', FromPort=22, ToPort=22)
    print('Done')
elif text == "No" or text =="no":
    print('Bye ! ')
