#Om een overzicht van alle VPC's en nieuwe VPC's te krijgen, wordt hier een functie van def_lib gebruikt. In vergelijking met "Basisch" waar de gebruiker de gegevens voor VPC wordt gevraagd, vermelden we hier drie componenten (VPC-naam, CIDR block VPC, CIDR block subnet) om vpc te maken.
from def_lib import vpc
vpc("TestNameTwo","192.168.0.0/16","192.168.1.0/24")

